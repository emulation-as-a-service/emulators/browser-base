from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

run sed -i.bak "/^# deb .*partner/ s/^# //" /etc/apt/sources.list 

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
unzip \
vde2 \
libvde-dev libvdeplug-dev libvdeplug2 libvde0 \
inotify-tools

run mkdir /libexec \
  && curl -o /libexec/eaas-proxy-download.zip -L "https://gitlab.com/emulation-as-a-service/eaas-proxy/-/jobs/artifacts/master/download?job=build" \
  && cd /libexec \
  && unzip eaas-proxy-download.zip \
  && mv eaas-proxy/eaas-proxy vdenode \
  && rm -r eaas-proxy \
  && mv vdenode eaas-proxy \
  && chmod +x eaas-proxy \
  && rm eaas-proxy-download.zip